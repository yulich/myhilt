package com.jeff.myhilt.di

import android.content.Context
import com.jeff.myhilt.vo.Sample
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.components.SingletonComponent

@EntryPoint
@InstallIn(SingletonComponent::class)
interface ExampleEntryPoint {
    fun fetchFoo(): String
    fun fetchInt(): Int
    fun fetchSample(): Sample
}

fun hiltExampleEntryPoint(context: Context): ExampleEntryPoint {
    val appContext = context.applicationContext
    return EntryPointAccessors.fromApplication(appContext, ExampleEntryPoint::class.java)
}

class HiltNotSupported(private val context: Context) {

    private val aFoo by lazy {
        hiltExampleEntryPoint(context).fetchFoo()
    }

    private val anInt by lazy {
        hiltExampleEntryPoint(context).fetchInt()
    }

    private val aSample by lazy {
        hiltExampleEntryPoint(context).fetchSample()
    }

    fun getTriple() = Triple(aFoo, anInt, aSample)
}