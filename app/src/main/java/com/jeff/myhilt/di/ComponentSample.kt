package com.jeff.myhilt.di

/**
 *  為 Android class 生成的組件
 *  https://developer.android.com/training/dependency-injection/hilt-android#generated-components
 */

/*
@Module
@InstallIn(Application::class)
class DataForApplication {
}

@Module
@InstallIn(ViewModelComponent::class)
class DataForViewModelComponent {
}

@Module
@InstallIn(ActivityComponent::class)
class DataForActivityComponent {
}

@Module
@InstallIn(FragmentComponent::class)
class DataForFragmentComponent {
}

@Module
@InstallIn(ViewComponent::class)
class DataForViewComponent {
}

@Module
@InstallIn(ServiceComponent::class)
class DataForServiceComponent {
}
*/
