package com.jeff.myhilt.di

import com.jeff.myhilt.vo.Sample
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Foo1

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Foo2

    @Singleton
    @Foo1
    @Provides
    fun provideFoo1() = "MyFoo1"

    @Singleton
    @Foo2
    @Provides
    fun provideFoo2() = "MyFoo2"

    @Singleton
    @Provides
    fun provideFoo3() = "MyFoo3"

    @Singleton
    @Provides
    fun provideFoo4() = 999

    @Singleton
    @Provides
    fun provideSample(@Foo1 p1: String, @Foo2 p2: String) = Sample(p1, p2)
}