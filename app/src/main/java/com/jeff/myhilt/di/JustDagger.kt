package com.jeff.myhilt.di

import com.jeff.myhilt.vo.Sample
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.hilt.migration.DisableInstallInCheck
import javax.inject.Inject

/**
 * @DisableInstallInCheck
 */

@Module
@DisableInstallInCheck
class OldWayModule {
    @Provides
    fun provideTest() = Sample("A", "B")
}

@Component(modules = [OldWayModule::class])
interface OldWayComponent {
    fun injectJeff(justDagger: OldWay)
}

class OldWay {
    init {
        val module = OldWayModule()
        DaggerOldWayComponent.builder().oldWayModule(module).build().injectJeff(this)
    }

    @Inject
    lateinit var sample: Sample
}