package com.jeff.myhilt.view.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.jeff.myhilt.R
import com.jeff.myhilt.base.BaseActivity
import com.jeff.myhilt.databinding.ActivityMainBinding
import com.jeff.myhilt.di.HiltNotSupported
import com.jeff.myhilt.di.OldWay
import com.log.JFLog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    private val viewModel: MainViewModel by viewModels()

    override fun getViewBinding(): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(findViewById(R.id.toolbar))

        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        // ViewModel
        viewModel.sample.also {
            JFLog.d("--- ViewModel ---")
            JFLog.d("${it}: ${it.p0}, ${it.p1}")
        }

        // Dagger 作法, 無法取得 Hilt 物件.
        OldWay().sample.also {
            JFLog.d("--- Dagger ---")
            JFLog.d("${it}: ${it.p0}, ${it.p1}")
        }

        // Hilt 物件注入自定義 class 方法.
        JFLog.d("--- Hilt ---")
        val diObject = HiltNotSupported(this)
        val result = diObject.getTriple()
        JFLog.d("${result.first}, ${result.second}")
        val sample = result.third
        JFLog.d("${sample}: ${sample.p0}, ${sample.p1}")
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}