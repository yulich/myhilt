package com.jeff.myhilt.view.main

import com.jeff.myhilt.base.BaseViewModel
import com.jeff.myhilt.vo.Sample
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject internal constructor(
    val sample: Sample
) : BaseViewModel()