package com.jeff.myhilt.view.page1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.jeff.myhilt.R
import com.jeff.myhilt.base.BaseFragment
import com.jeff.myhilt.databinding.FragmentFirstBinding
import com.jeff.myhilt.vo.Sample
import com.log.JFLog
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class FirstFragment : BaseFragment<FragmentFirstBinding>() {

    private val viewModel by viewModels<FirstFragmentViewModel>()

    @Inject
    lateinit var sample: Sample

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.button_first).setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
        }

        JFLog.d("$sample")
        JFLog.d("${viewModel.sample}")
        JFLog.d("${viewModel.generalObj}: ${viewModel.generalObj.foo1}")
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentFirstBinding {
        return FragmentFirstBinding.inflate(inflater, container, false)
    }
}