package com.jeff.myhilt.view.page1

import com.jeff.myhilt.base.BaseViewModel
import com.jeff.myhilt.vo.GeneralObj
import com.jeff.myhilt.vo.Sample
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FirstFragmentViewModel @Inject internal constructor(
    val generalObj: GeneralObj,
    val sample: Sample,
) : BaseViewModel()