package com.jeff.myhilt.vo

import android.content.Context
import com.jeff.myhilt.di.AppModule
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class GeneralObj @Inject internal constructor(
    @ApplicationContext val context: Context,
    @AppModule.Foo1 val foo1: String,
)