[Overview] (https://developer.android.com/training/dependency-injection)

[Manual dependency injection] (https://developer.android.com/training/dependency-injection/manual)

[Dependency injection with Hilt] (https://developer.android.com/training/dependency-injection/hilt-android)

[Dagger-Hilt] (https://dagger.dev)